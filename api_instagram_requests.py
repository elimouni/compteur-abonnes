# Importing the needed lib
import requests
from time import sleep

def instagram_subs_amount(username):
    # Query to get the channel's ID
    request = 'https://www.instagram.com/' + username + '/?__a=1'

    try:
        nb_followers = requests.get(request).json()['graphql']['user']['edge_followed_by']['count']
    except requests.exceptions.ConnectionError:
        sleep(1)
        nb_followers = instagram_subs_amount(username)

    return nb_followers
