import os
import re
import time
import argparse

from luma.led_matrix.device import max7219
from luma.core.interface.serial import spi, noop
from luma.core.render import canvas
from luma.core.virtual import viewport
from luma.core.legacy import text, show_message
from luma.core.legacy.font import proportional, CP437_FONT, TINY_FONT, SINCLAIR_FONT, LCD_FONT

from math import floor
from pathlib import Path
from dotenv import load_dotenv
from api_instagram_requests import instagram_subs_amount
from api_twitch_requests import twitch_subs_amount
from api_youtube_requests import youtube_subs_amount


SLEEP_TIME = 5

def display_msg(device, letter='E', amount='00'):
    msg = str(letter) + ':' + str(amount)

    # Displaying message
    with canvas(device) as draw:
        text(draw, (0, 0), msg, fill="white", font=proportional(LCD_FONT))


def crop_number(number):
    nb_digits = len(str(number))

    if nb_digits > 6:
        number = str(floor(number/10000)/100) + 'M'
    elif nb_digits > 5:
        number = str(floor(number/1000)) + 'k'
    elif nb_digits > 4:
        number = str(floor(number/100)/10) + 'k'
    elif nb_digits > 3:
        number = str(floor(number/10)/100) + 'k'
    else:
        number = str(number)

    return number


def main():
    # Load conf
    env_path = Path('.') / '.env'
    load_dotenv(dotenv_path=env_path, verbose=True)
    youtube_api_key = os.getenv("YOUTUBE_API_KEY")
    youtube_channel_id = os.getenv("YOUTUBE_CHANNEL_ID")
    twitch_key = os.getenv("TWITCH_CLIENT_ID")
    twitch_channel_name = os.getenv("TWITCH_CHANNEL_NAME")
    instagram_username = os.getenv("INSTAGRAM_USERNAME")

    # create matrix device
    serial = spi(port=0, device=0, gpio=noop())
    device = max7219(serial, cascaded=4, block_orientation=+90,
                     rotate=0, blocks_arranged_in_reverse_order=True)

    time.sleep(3)
    while True:
        youtube = crop_number(int(youtube_subs_amount(youtube_api_key, youtube_channel_id)))
        time.sleep(SLEEP_TIME)
        display_msg(device, 'Y', youtube)
        twitch = crop_number(int(twitch_subs_amount(twitch_key, twitch_channel_name)))
        time.sleep(SLEEP_TIME)
        display_msg(device, 'T', twitch)
        instagram = crop_number(int(instagram_subs_amount(instagram_username)))
        time.sleep(SLEEP_TIME)
        display_msg(device, 'I', instagram)


main()
