# Importing the needed libs
import requests
from time import sleep

def youtube_subs_amount(key, channel_id):
    # Query to get the channel's data
    request = 'https://www.googleapis.com/youtube/v3/channels?part=statistics&id=' + str(channel_id) + '&key=' + str(key)

    # Execution of the query and extraction of the subscribers amount
    try:
        nb_followers = requests.get(request).json()['items'][0]['statistics']['subscriberCount']
    except requests.exceptions.ConnectionError:
        sleep(1)
        nb_followers = youtube_subs_amount(key, channel_id)

    return nb_followers
