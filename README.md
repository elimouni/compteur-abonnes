# Subscriber amount display for YouTube, Instagram and Twitch

## Hardware

### Hardware needed

You will need to create this simple project :
- a Raspberry Pi (model 3, but Zero are supported too)
- a power supplier for the Raspberry Pi
- a 256 LED matrix (MAX7219 is used here)
- 5 wires to connected the LED matrix to the Raspberry Pi
- a 3D printer, or some materials to make a clean case

### Electronical part

This part is really simple to do. Just wire the pins one the LED matrix to the RPi like this :

|**MAX7219**|**Raspberry Pi**|
|:---------:|:--------------:|
|VCC        | 2              |
|GND        | 6              |
|DIN        | 19             |
|CS         | 24             |
|CLK        | 23             |

## Software

### Installation of the needed libraries

Start by installing Raspbian OS, or another OS, on your Raspberry Pi. A lot of tutorials are available on the internet. Then, connect to the internet, using Wifi or by wiring the Ethernet interface.

Then, install Python 3.X on your Raspberry Pi by using the command on Raspbian OS :

```shell
sudo apt-get install python3
```

After that, install the needed libraries by using :

```shell
sudo pip3 install -U dotenv luma requests
```

If you miss another library, just install it.

### Installation of the python program

Now, you can install the program. Start by cloning the repository where you want. I personnally install it in /home/pi/Desktop.

```shell
git clone https://gitlab.com/elimouni/compteur-abonnes.git
```

Go inside the project folder :

```shell
cd compteur-abonnes
```

If you launch the main.py script, you will have an error. You will need to create the **.env** file. This file is the configuration file.

Copy this text :

```
INSTAGRAM_USERNAME=
TWITCH_CLIENT_ID=
TWITCH_CHANNEL_NAME=
YOUTUBE_API_KEY=
YOUTUBE_CHANNEL_ID=
```

Then, use this command to create the .env file and paste the text in it.

```
sudo nano .env
```

You will need to fill the values in this parameter file. This is a table with explanation on where you can find the values to add.

|Value in the .env file|Explanation                             |
|:--------------------:|:--------------------------------------:|
|INSTAGRAM_USERNAME    |This is just your username on Instagram.|
|TWITCH_CLIENT_ID      |Get it from the [Twitch Dev Console](https://dev.twitch.tv/console/apps/create "Twitch Dev Console")|
|TWITCH_CHANNEL_NAME   |This is your Twitch channel's name.     |
|YOUTUBE_API_KEY       |Get it from the [Google API console](https://console.developers.google.com/apis/credentials "Google API Console"), by creating your app and activating YouTube API v3.|
|YOUTUBE_CHANNEL_ID    |Get it from your [YouTube account advanced options](https://www.youtube.com/account_advanced "YouTube account advanced options")|

Now, save the .env file and start the program to check if everything is working :

```shell
python3 main.py
```

If everything is working, the matrix should now show how many followers you have on Twitch, YouTube, and Instagram.

### Creating the service to start the program at boot

This part will auto start the python program when you boot the Raspberry Pi. We will use **systemd** to do it.

Start by going into the system folder :

```shell
cd /etc/systemd/system
```

Then, create your system file by copying this text :

```
[Unit]
Description=Followers Counter Display
After=network-online.target
Wants=network-online.target

[Service]
Restart=on-abort
WorkingDirectory=
ExectStart=/usr/bin/python3 main.py

[Install]
WantedBy=multi-user.target
```

And paste it here after creating the service file :

```shell
sudo nano counter.service
```
Fill the WorkingDirectory folder, which is the path to the project folder.

If Python 3 is installed in another location than /usr/bin/python3, change it to have the good path.

Then, enable the service, start it, and check its status :

```shell
systemctl enable counter.service
systemctl start counter.service
systemctl status counter.service
```

Now, everything should work. You can check it by rebooting the RPi :

```shell
sudo reboot
```

## Putting everything in a case to have a nice clean counter

This part isn't created actually... maybe soon or by one of you !

# Thanks to ...
- guillaume for helping me to use the twitch API
- electrautistic for helping me to use systemd

Don't hesitate to follow me on [Twitch](https://twitch.tv/eliaccess), where I create projects with my tchat (french channel ... but I have some knowledge is english !) aand to improve this project by forking it !
