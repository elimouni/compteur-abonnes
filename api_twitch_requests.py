# Importing the needed libs
# Ty à Guillaume for the help
import requests


def twitch_subs_amount(key, channel_name):
    headers = {'Client-ID': key, 'Accept': 'application/vnd.twitchtv.v5+json'}

    # Query to get the channel's ID
    request = 'https://api.twitch.tv/kraken/users?login=' + channel_name

    try:
        channel_id = requests.get(request, headers=headers).json()['users'][0]['_id']
    except requests.exceptions.ConnectionError:
        return 1

    request = 'https://api.twitch.tv/kraken/channels/' + channel_id + '/follows'
    # Execution of the query and extraction of the subscribers amount
    try:
        nb_followers = requests.get(request, headers=headers).json()['_total']
    except requests.exceptions.ConnectionError:
        sleep(1)
        nb_followers = twitch_subs_amount(key, channel_name)

    return nb_followers
